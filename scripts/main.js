
async function findLocation() {
    const ipResponse = await fetch('https://api.ipify.org/?format=json');
    const { ip } = await ipResponse.json();
    
    const locationResponse = await fetch(`http://ip-api.com/json/${ip}`);
    
    const location = await locationResponse.json();
    const locationItems = ['continent', 'country', 'regionName', 'city', 'region'];
    
    for (let key in location) {
     if(locationItems.includes(key) && location.hasOwnProperty(key)) {
      const value = location[key];
      const element = document.createElement("p");
     
      element.insertAdjacentHTML(
        'afterbegin',
       ` <span class="title">${key}</span>:
        <span class="info">${value}</span>`
      );

      document.getElementById('location-info').prepend(element);
     }
    }
}



